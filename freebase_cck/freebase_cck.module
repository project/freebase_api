<?php
/**
 * @file Freebase to CCK
 *
 * Utility to import FreeBase Topic schemas as CCK content types.
 *
 * Designed to be used with 'cck_importer', it declares the services, subform
 * elements and formats that cck_importer UI uses to create its forms.
 *
 * The data conversion is done by freebase_cck.schema.inc)
 *
 *
 * This tool will make a request to Freebase and retrieve 'Topic' schemas which
 * are equivalent to our CCK content types.
 *
 * It converts those Freebase schemas into form elements we can use to define
 * new types. The module publishes a few methods that are used by cck_importer.
 * module that handles the actual importing and creation.
 *
 * @author 'dman' Dan Morrison http://coders.co.nz/
 * @version 2010
 */

/**
 * Load the conversion routines. This is currently owned by freebase_api, but
 * only really used by me here.
 * TODO - think about where this lives, where freebase API things go.
 */
module_load_include('inc', 'freebase_cck', 'freebase_cck.schema');

/**
 * Return descriptions of this service for CCK_Schema to know about
 *
 * Implementation of HOOK_field_schema_importer_services() from cck_importer.module
 *
 */
function freebase_cck_field_schema_importer_services() {
  $services = array();
  $services['freebase-topic'] = array(
    'provider' => 'Freebase',
    'name' => 'Freebase Topic type',
    'id' => 'freebase-topic',
    'about' => FB_URI,
    'description' => '
      Freebase describes a number of "topic" types to store information about
      a subject. We can import each of the field definitions attached to a
      Freebase topic as a field API field.
    ',
    // Define the name of the form function that returns service-specific UI
    // form that gets displayed for this service
    'import_form_callback' => 'freebase_cck_form',
    // function that can take parameter and return the raw response
    'request_callback' => 'freebase_api_fetch_schema_definition',
    // format that the raw response will be in
    'conversion_format' => 'freebase_json',
    // function that performs the format conversion. Takes a string, returns a PHP array
    'conversion_callback' => 'freebase_api_schema_to_cck_content_type'
  );
  return $services;
}

/**
 * Return a list of formats we can handle.
 *
 * Implementation of HOOK_field_schema_importer_formats() from field_schema_importer.module
 */
function freebase_cck_field_schema_importer_formats() {
  $formats = array(
    'freebase_json' => array(
      'id' => 'freebase_json',
      'name' => 'FreeBase JSON',
      'module' => 'freebase_cck',
      // Name of the function that can read this type of input and return a PHP array
      'conversion_callback' => 'freebase_api_json_to_cck_content_type',
    ),
  );
  return $formats;
}

/**
 * Form UI for communicating with freebase and choosing a type defintion to
 * load.
 *
 * This form is named above as a callback in
 * freebase_cck_cck_importer_services()
 *
 * This is a SUB-form, that gets included from something higher.
 *
 * It presents a selection method for choosing what to import from Freebase.
 *
 * On submission, it may be rebuilt to include the import preview form also
 */
function freebase_cck_form(&$form_state, $service_info) {
  $service_id = $service_info['id']; // 'freebase_cck';
  $form_values = @$form_state['values'];
  $our_form_values = @$form_values[$service_id];
  #dpm(get_defined_vars());

  drupal_add_js(drupal_get_path('module', 'freebase_cck') . '/freebase_cck.js');
  $form = array();
  $form = array(
    '#type' => 'fieldset',
    '#title' => $service_info['name'],
  );

  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
  );

  $form['search']['chosen_type'] = array(
    '#title' => 'Type definition name',
    '#type' => 'textfield',
    '#default_value' => $our_form_values['search']['chosen_type'] ? $our_form_values['search']['chosen_type'] : variable_get($service_id . "-type", ""),
    '#attributes' => array('class' => 'chosen-type'),
    // Add an extra class so jquery can find it, don't trust IDs
  );
  // We really expect to use freebase suggest, but CAN run without it
  if (module_exists('jquery_freebase')) {
    // Attach suggest widget to this element
    $form['search']['chosen_type'] += array(
      '#process' => array('jquery_freebase_attach_autocomplete_suggest'),
      '#freebase_suggest_arguments' => '{"type": "/type/type"}',
      '#description' => t("
        Enter or select a type of object from the Freebase type profiles
      "),
    );
  }
  else {
    $form['search']['chosen_type'] += array(
      '#description' => t("
        As you <em>don't</em> have freebase suggest
        (provided by !jquery_freebase)
        You must enter a valid Freebase Type ID manually. eg
        <br/>
        <code>/book/book</code>
        <br/>
        <code>/people/person</code>
        <br/>
        It's a lot more fun if you enable !jquery_freebase, so do that instead.
      ", array(
          '!jquery_freebase' => l('jquery_freebase.module', 'http://drupal.org/project/jquery_freebase'),
        )
      )
    );
  }

  // May hide or small this field
  $form['search']['chosen_type_id'] = array(
    '#title' => 'Type ID',
    '#type' => 'textfield',
    '#default_value' => $our_form_values['search']['chosen_type_id'], // variable_get($service_id . "_type_id", ""),
    '#attributes' => array('class' => 'chosen-type-id'), // so jquery can find it, don't trust IDs
  );

  if (!module_exists('rdf_mapping')) {
    $form['search']['rdf_mapping'] = array(
      '#type' => 'markup',
      '#value' => t("
        I can import schemas for you, but to helpfully remember the semantics
        of the data types we are using, you really would be best to also enable
        rdf_mapping.module before you continue!
      "),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    );
  }

  // In step 2,
  // Build the CCK add form here
  // This will not show up in the normal case, but it is part of the form
  // that will be submitted later, and we declare it here so that AJAX will
  // load it without complaining about newly added fields

  $form['search']['load_schema'] = array(
    '#type' => 'submit',
    '#value' => t('Load Schema'),
    '#validate' => array('freebase_cck_form_validate'),
  );

  // If the first button (retrieve and parse) was pressed,
  // retrieve and populate the all-in-one form
  if (!empty($our_form_values['search']['chosen_type_id'])) {
    // A valid ID was set,

    // Collapse the search
    $form['search']['#collapsible'] = TRUE;
    $form['search']['#collapsed'] = TRUE;

    if ($form_values['op'] == t('Load Schema') ) {

      // Now get the data from freebase
      $type_id = $our_form_values['search']['chosen_type_id'];
      $schema_definition = freebase_api_fetch_schema_definition($type_id);

      if (empty($schema_definition)) {
        drupal_set_message(t("Failed to retrieve a !schema_definition for !type_id from !freebase.", array('!type_id' => l($type_id, 'http://www.freebase.com/view' . $type_id), '!freebase' => l('Freebase', 'http://freebase.com'), '!schema_definition' => l('schema definition', 'http://www.freebase.com/type/schema'. $type_id))), 'error');
      }
      else {
        // Convert it to something like a cck definition
        $content = freebase_api_schema_to_cck_content_type($schema_definition);
        // This is now an array as used by CCK import.
        if (CCK_IMPORTER_DEBUG) {
          // DEBUG
          $form['search']['freebase_schema_json'] = array(
            '#title' => 'The retrieved json data schema',
            '#type' => 'textarea',
            '#default_value' => '$content = '. var_export($schema_definition, 1) . ';',
            '#description' => t('This is what Freebase gave us'),
          );
          $form['search']['content_type_export'] = array(
            '#title' => 'The preliminary CCK export',
            '#type' => 'textarea',
            '#default_value' => '$content = '. var_export($content, 1) . ';',
            '#description' => t('This could be pasted into the CCK import form as-is, but you can tune some values here first...'),
          );
        }

        // Render an all-in-one CCK editor
        // To do this, pass rendering control back to the cck_importer generic form builder
        $form['content_type'] = field_schema_importer_content_type_form($form_state, $content);
      }

    }
  }
  return $form;
}

/**
 * Checks before attempting to load a freebase schema.
 *
 * Called when the load_schema button was pressed within the freebase source
 * selector.
 * Ensure that a valid freebase chosen_type_id identifier is present
 *
 * Can't just make it required as it depends on higher choices
 *
 * This is called from #element_validate, so the $form context is just the
 * freebase input fieldset
 *
 * @see freebase_cck_form()
 */
function freebase_cck_form_validate($form, &$form_state) {
  $form_values = $form_state['values'];
  $service_id = $form_values['service_id'];
  $our_form_values = $form_values[$service_id];
  if (empty($our_form_values['search']['chosen_type_id'])) {
    form_set_error("$service_id][chosen_type_id", t('Please enter a Freebase topic identifier, eg /book/book'));
    #dpm(get_defined_vars());
  }
}

